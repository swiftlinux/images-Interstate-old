This repository contains the images used for the Interstate Swift Linux wallpaper.

Logo image source: https://simple.wikipedia.org/wiki/List_of_Interstate_Highways#/media/File:I-blank.svg

Desktop image source: https://upload.wikimedia.org/wikipedia/commons/archive/d/d9/20180701040646%2185_miles_per_hour_speed_limit_on_Texas_highway_130.jpg
